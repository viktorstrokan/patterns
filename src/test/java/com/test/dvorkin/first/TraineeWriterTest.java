package com.test.dvorkin.first;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TraineeWriterTest {
    private static final String BASE_DIR = "C:\\Users\\Viktor\\IdeaProjects\\patterns\\src\\test\\resources\\";

    private TraineeWriter writer;

    @Before
    public void setUp() throws Exception {
        writer = new TraineeWriter();
    }

    @Test //11
    public void testWriteToFileMultiLine() {
        Trainee trainee = new Trainee();
        trainee.setFirstName("Abc");
        trainee.setLastName("Xyz");
        trainee.setMark(3);

        writer.writeToFileMultiLine(BASE_DIR + "traineeMultiLine", trainee);
    }

    @Test //12
    public void testReadFromFileMultiLine() {
        Trainee trainee = new Trainee();
        trainee.setFirstName("Abc");
        trainee.setLastName("Xyz");
        trainee.setMark(3);
        writer.writeToFileMultiLine(BASE_DIR + "traineeMultiLineRead", trainee);

        Trainee readTrainee = writer.readFromFileMultiLine(BASE_DIR + "traineeMultiLineRead");

        assertEquals(trainee, readTrainee);
    }

    @Test //13
    public void testWriteToFileSingleLine() {
        Trainee trainee = new Trainee();
        trainee.setFirstName("Abc");
        trainee.setLastName("Xyz");
        trainee.setMark(3);

        writer.writeToFileSingleLine(BASE_DIR + "traineeSingleLine", trainee);
    }

    @Test //14
    public void testReadFromFileSingleLine() {
        Trainee trainee = new Trainee();
        trainee.setFirstName("Abc");
        trainee.setLastName("Xyz");
        trainee.setMark(3);
        writer.writeToFileSingleLine(BASE_DIR + "traineeSingleLineRead", trainee);

        Trainee readTrainee = writer.readFromFileSingleLine(BASE_DIR + "traineeSingleLineRead");

        assertEquals(trainee, readTrainee);
    }

    @Test //15
    public void testSerializeTraineeBinary() {
        Trainee trainee = new Trainee();
        trainee.setFirstName("Abc");
        trainee.setLastName("Xyz");
        trainee.setMark(3);

        writer.serializeTraineeBinary(BASE_DIR + "traineeBinary", trainee);
        Trainee readTrainee = writer.deserializeTraineeBinary(BASE_DIR + "traineeBinary");

        assertEquals(readTrainee, trainee);
    }

    @Test //16
    public void testSerializeTraineeBinaryByte() {
        Trainee trainee = new Trainee();
        trainee.setFirstName("Abc");
        trainee.setLastName("Xyz");
        trainee.setMark(3);

        writer.serializeTraineeBinaryByte(BASE_DIR + "traineeBinaryByte", trainee);
        Trainee readTrainee = writer.deserializeTraineeBinaryByte(BASE_DIR + "traineeBinaryByte");

        assertEquals(readTrainee, trainee);
    }

    @Test //17
    public void testTraineeToJsonString() {
        Trainee trainee = new Trainee();
        trainee.setFirstName("Abc");
        trainee.setLastName("Xyz");
        trainee.setMark(3);

        String json = writer.traineeToJsonString(trainee);
        Trainee readTrainee = writer.traineeFromJsonString(json);

        assertEquals(readTrainee, trainee);
    }

    @Test
    public void testTraineeToJsonFile() {
        Trainee trainee = new Trainee();
        trainee.setFirstName("Abc");
        trainee.setLastName("Xyz");
        trainee.setMark(3);

        writer.traineeToJsonFile(trainee, BASE_DIR + "trainee.json");
        Trainee readTrainee = writer.traineeFromJsonFile(BASE_DIR + "trainee.json");

        assertEquals(readTrainee, trainee);
    }
}