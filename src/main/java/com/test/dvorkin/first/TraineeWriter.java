package com.test.dvorkin.first;

import com.google.gson.Gson;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TraineeWriter {
    //11
    public void writeToFileMultiLine(String path, Trainee trainee) {
        List<String> lines = traineeToLines(trainee);
        try {
            Files.write(Paths.get(path), lines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //12
    public Trainee readFromFileMultiLine(String path) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(path));
            return linesToTrainee(lines);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //13
    public void writeToFileSingleLine(String path, Trainee trainee) {
        String line = traineeToLine(trainee);
        try {
            Files.write(Paths.get(path), Collections.singletonList(line));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //14
    public Trainee readFromFileSingleLine(String path) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(path));
            return lineToTrainee(lines.get(0));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //15
    public void serializeTraineeBinary(String path, Trainee trainee) {
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))) {
            oos.writeObject(trainee);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //15
    public Trainee deserializeTraineeBinary(String path) {
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path))) {
            return (Trainee) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    //16
    public void serializeTraineeBinaryByte(String path, Trainee trainee) {
        try(ByteArrayOutputStream baos = new ByteArrayOutputStream();
            OutputStream outputStream = new FileOutputStream(path)) {
            baos.write(trainee.getFirstName().getBytes(StandardCharsets.UTF_8));
            baos.write(";".getBytes(StandardCharsets.UTF_8));
            baos.write(trainee.getLastName().getBytes(StandardCharsets.UTF_8));
            baos.write(";".getBytes(StandardCharsets.UTF_8));
            baos.write(String.valueOf(trainee.getMark()).getBytes());
            baos.writeTo(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //16
    public Trainee deserializeTraineeBinaryByte(String path) {
        List<Byte> content = new ArrayList<>();

        byte[] buffer = new byte[10];

        try (ByteArrayInputStream bais = new ByteArrayInputStream(Files.readAllBytes(Paths.get(path)))) {
            int size = bais.available();
            char[] theChars = new char[size];
            byte[] bytes    = new byte[size];

            bais.read(bytes, 0, size);
            for (int i = 0; i < size;)
                theChars[i] = (char)(bytes[i++] & 0xff);

            String x = new String(theChars);
            String[] split = x.split(";");
            if (split.length != 3) {
                throw new IllegalStateException("Trainee must contain 3 parts. Actual parts: " + split.length);
            }
            Trainee trainee = new Trainee();
            trainee.setFirstName(split[0]);
            trainee.setLastName(split[1]);
            trainee.setMark(Integer.parseInt(split[2]));
            return trainee;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //17
    public String traineeToJsonString(Trainee trainee) {
        Gson gson = new Gson();
        return gson.toJson(trainee);
    }

    //17
    public Trainee traineeFromJsonString(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Trainee.class);
    }

    //18
    public void traineeToJsonFile(Trainee trainee, String path) {
        Gson gson = new Gson();
        try(FileWriter fw = new FileWriter(path)) {
            gson.toJson(trainee, fw);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //18
    public Trainee traineeFromJsonFile(String path) {
        Gson gson = new Gson();
        try (FileReader fr = new FileReader(path)) {
            return gson.fromJson(fr, Trainee.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Trainee lineToTrainee(String line) {
        if (line == null || line.isEmpty()) {
            throw new IllegalStateException("Can't read trainee from empty line");
        }

        String[] split = line.split(";");
        if (split.length != 3) {
            throw new IllegalStateException("There must be 3 parts to build trainee. Parts found: " + split.length);
        }

        Trainee trainee = new Trainee();
        trainee.setFirstName(split[0]);
        trainee.setLastName(split[1]);
        trainee.setMark(Integer.parseInt(split[2]));
        return trainee;
    }

    private List<String> traineeToLines(Trainee trainee) {
        List<String> lines = new ArrayList<>();
        lines.add(trainee.getFirstName());
        lines.add(trainee.getLastName());
        lines.add(String.valueOf(trainee.getMark()));

        return lines;
    }

    private Trainee linesToTrainee(List<String> lines) {
        if (lines == null || lines.size() != 3) {
            throw new IllegalStateException("Trainee must be read from 3 lines");
        }

        Trainee trainee = new Trainee();
        trainee.setFirstName(lines.get(0));
        trainee.setLastName(lines.get(1));
        trainee.setMark(Integer.parseInt(lines.get(2)));
        return trainee;
    }

    private String traineeToLine(Trainee trainee) {
        return trainee.getFirstName() +
                ";" +
                trainee.getLastName() +
                ";" +
                trainee.getMark();
    }
}
