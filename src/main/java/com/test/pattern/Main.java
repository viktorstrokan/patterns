package com.test.pattern;

import com.test.pattern.prototype.BisquitCookie;
import com.test.pattern.prototype.Cookie;
import com.test.pattern.prototype.Cracker;
import com.test.pattern.proxy.AbstractCar;
import com.test.pattern.proxy.Lada;
import com.test.pattern.proxy.SafeCar;
import com.test.pattern.singleton.Kremlin;

public class Main {
    public static void main(String[] args) {
        testSingleton();
        testPrototype();
        testProxy();
    }

    private static void testSingleton() {
        Kremlin kremlin = Kremlin.getInstance();
        Kremlin kremlin2 = Kremlin.getInstance();
        System.out.println("Is the same Kremlin: " + (kremlin == kremlin2));
    }

    private static void testPrototype() {
        BisquitCookie prototypeBisquit = new BisquitCookie();
        Cookie bisquitCopy = prototypeBisquit.clone();
        System.out.println("Copy of bisquit cookie is the same: " + prototypeBisquit.equals(bisquitCopy));

        Cracker prototypeCracker = new Cracker();
        Cookie crackerCopy = prototypeCracker.clone();
        System.out.println("Copy of cracker cookie is the same: " + prototypeCracker.equals(crackerCopy));
    }

    private static void testProxy() {
        AbstractCar lada = new Lada();
        lada.startEngine();

        AbstractCar safeLada = new SafeCar(lada);
        try {
            safeLada.startEngine();
        } catch (IllegalStateException ex) {
            System.out.println("Engine was not started: " + ex.getMessage());
        }
    }
}
