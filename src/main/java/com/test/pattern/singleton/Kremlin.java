package com.test.pattern.singleton;

public class Kremlin {
    private static Kremlin instance;

    private Kremlin() {
    }

    public static Kremlin getInstance() {
        if (instance == null) {
            instance = new Kremlin();
        }
        return instance;
    }
}
