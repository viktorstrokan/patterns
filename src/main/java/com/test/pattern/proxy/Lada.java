package com.test.pattern.proxy;

public class Lada extends AbstractCar {
    @Override
    public void startEngine() {
        System.out.println("Lada has started engine!");
    }
}
