package com.test.pattern.proxy;

public abstract class AbstractCar implements Car {
    private boolean seatBeltFastened;

    public boolean isSeatBeltFastened() {
        return seatBeltFastened;
    }

    public void setSeatBeltFastened(boolean seatBeltFastened) {
        this.seatBeltFastened = seatBeltFastened;
    }
}
