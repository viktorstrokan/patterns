package com.test.pattern.proxy;

public interface Car {
    void startEngine();
}
