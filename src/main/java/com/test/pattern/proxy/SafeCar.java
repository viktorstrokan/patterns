package com.test.pattern.proxy;

public class SafeCar extends AbstractCar {
    private final AbstractCar car;

    public SafeCar(AbstractCar car) {
        this.car = car;
    }

    @Override
    public void startEngine() {
        if (!car.isSeatBeltFastened()) {
            throw new IllegalStateException("You must fasten the seat belt before starting the engine!");
        }
        car.startEngine();
    }
}
