package com.test.pattern.prototype;

import java.util.Objects;

public class Cracker extends Cookie {
    private boolean salty;

    public Cracker() {
    }

    public Cracker(Cracker origin) {
        super(origin);
        if (origin != null) {
            this.salty = origin.salty;
        }
    }

    public boolean isSalty() {
        return salty;
    }

    public void setSalty(boolean salty) {
        this.salty = salty;
    }

    @Override
    public Cookie clone() {
        return new Cracker(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Cracker cracker = (Cracker) o;
        return salty == cracker.salty;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), salty);
    }
}
