package com.test.pattern.prototype;

import java.util.Objects;

public abstract class Cookie {
    private String name;
    private int weight;

    public Cookie() {
    }

    public Cookie(Cookie origin) {
        if (origin != null) {
            this.name = origin.name;
            this.weight = origin.weight;
        }
    }

    public abstract Cookie clone();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cookie cookie = (Cookie) o;
        return weight == cookie.weight &&
                Objects.equals(name, cookie.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, weight);
    }
}
