package com.test.pattern.prototype;

import java.util.Objects;

public class BisquitCookie extends Cookie {
    private String bisquitType;

    public BisquitCookie() {
    }

    public BisquitCookie(BisquitCookie origin) {
        super(origin);
        if (origin != null) {
            this.bisquitType = origin.bisquitType;
        }
    }

    public String getBisquitType() {
        return bisquitType;
    }

    public void setBisquitType(String bisquitType) {
        this.bisquitType = bisquitType;
    }

    @Override
    public Cookie clone() {
        return new BisquitCookie(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BisquitCookie that = (BisquitCookie) o;
        return Objects.equals(bisquitType, that.bisquitType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), bisquitType);
    }
}
